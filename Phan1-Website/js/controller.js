let formatter = new Intl.NumberFormat("vn-VN", {
  style: "currency",
  currency: "VND",
});

export function onLoading() {
  document.getElementById('loading').style.display = 'flex';
}
export function offLoading() {
  document.getElementById('loading').style.display = 'none';
}


export function renderProduct(list) {
  let content = ''

  list.forEach((sp) => {
    content += /*HTML*/`
          <li class="card product__item" style="width: 18rem">
              <div class="img__item">
                <img
                  src="${sp.img}"
                  class="card-img-top"
                  alt="..."
                />
              </div>
              <div class="card-body">
                <h3 class="card-title">${sp.name}</h3>
                <div class="product__desc">
                  <p>${sp.desc}</p>
                  <p>Screen: ${sp.screen}</p>
                  <p>Camera trước: ${sp.frontCamera}</p>
                  <p>Camera sau: ${sp.backCamera}</p>
                  <strong>${formatter.format(sp.price)}</strong >
                </div >
              <button onclick="addToCart(${sp.id})" class="btn custom__btn">Thêm vào giỏ hàng</button>
              </div >
            </li >
        `
  })
  document.getElementById('listProduct').innerHTML = content
}

export function renderCart(list) {
  let totalQuantity = 0
  let totalPrice = 0
  let content = ''
  let cartTableBody = document.getElementById('tBodyCart')
  list.forEach(itemCart => {
    let price = itemCart.product.price * itemCart.quantity
    // tổng số lượng sản phẩm
    totalQuantity += itemCart.quantity

    // TỔng tiền sản phẩm
    totalPrice += price

    content += /*HTML*/`
    
    <tr>
    <td  class="text-center">
    <img style="width:70px" src="${itemCart.product.img}">
    </td>
    <td class="align-middle">${itemCart.product.name}</td>
    <td class="align-middle text-center">
    <button id="prev" onclick="decrement(${itemCart.product.id})" class="buttonQuantity "><i class="fa fa-angle-left"></i></button>
    ${itemCart.quantity}
    <button id="next" onclick="increment(${itemCart.product.id})" class="buttonQuantity "><i class="fa fa-angle-right"></i></button>

    </td>
    <td class="align-middle text-center">${formatter.format(price)}</td>
    <td onclick="deteleProduct(${itemCart.product.id})" class="align-middle text-center"><button style="border: none;background: transparent;color: white;"><i class="fa fa-trash"></i></button></td>
  </tr>
    `
  })
  if (list.length > 0) {
    cartTableBody.innerHTML = content
    document.getElementById('tableCart').style.display = 'table'
    document.getElementById('empty__cart').style.display = 'none'

  } else {
    document.getElementById('tableCart').style.display = 'none'
    document.getElementById('empty__cart').innerHTML = `<h3 style="text-align: center;margin: 50px 0">Bạn chưa có sản phẩm trong giỏ hàng</h3>`
    document.getElementById('empty__cart').style.display = 'block'


  }
  document.getElementById('quantity').innerHTML = totalQuantity
  document.getElementById('totalPrice').innerHTML = formatter.format(totalPrice)

}

export function removeAllItems(list) {
  list.splice(0, list.length)
}





