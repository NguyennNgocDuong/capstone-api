import { offLoading, onLoading, removeAllItems, renderCart, renderProduct } from "./controller.js";
import { CartItem } from "./productsModel.js";


let productList = []
const BASE_URL = 'https://62f8b754e0564480352bf3cc.mockapi.io'





function renderProductServices() {
  onLoading()
  axios({
    url: `${BASE_URL}/sp`,
    method: 'GET'
  })
    .then(res => {
      offLoading()
      productList = res.data

      renderProduct(productList)


    })
    .catch(err => {
      offLoading()

      console.log('err: ', err);

    })
}
renderProductServices()

window.filterPhone = filterPhone
function filterPhone() {


  let selectProduct = document.getElementById('selectTypePhone').value;


  if (selectProduct == "Tất cả") {
    renderProduct(productList)

  } else {
    let filterProduct = productList.filter((item) => {


      return item.type.toUpperCase() == selectProduct.toUpperCase()

    })
    if (filterProduct.length > 0) {
      document.getElementById('noticeEmpty').innerHTML = ''
      renderProduct(filterProduct)
    } else {
      document.getElementById('noticeEmpty').innerHTML = `<h2>Hiện chưa có sản phẩm</h2>`
      renderProduct(filterProduct)

    }
  }

}


/** 
 *  cartItem = {
 *      product:{id:1,name:aac},
 *      quantity:1
 * }
*/
let cart = []


let cartStorage = localStorage.getItem('cart')
if (cartStorage) {
  cart = JSON.parse(cartStorage)
  renderCart(cart)
}

function saveLocalStorage() {
  localStorage.setItem('cart', JSON.stringify(cart))

}



window.addToCart = addToCart
function addToCart(id) {

  axios({
    url: `${BASE_URL}/sp/${id}`,
    method: 'GET',
  })
    .then(res => {
      let product = res.data

      // tìm trong mảng cart, item nào có id = với id sản phẩm
      let item = cart.find(c => c.product.id == id)
      // nếu có item thì tăng số lượng
      if (item) {
        item.quantity += 1

      }
      // nếu chưa có thì push itemCart vào mảng cart
      else {
        let cartItem = new CartItem(product, 1)
        cart.push(cartItem)
      }
      // save localstorage cart sau khi push
      saveLocalStorage()
      renderCart(cart)
    })
    .catch(err => {
      console.log('err: ', err);

    })
}

window.increment = increment
function increment(id) {
  let item = cart.find(c => c.product.id == id)

  if (item) {
    item.quantity++
    renderCart(cart)
    saveLocalStorage()
  }

}

window.decrement = decrement
function decrement(id) {
  let item = cart.find(c => c.product.id == id)

  if (item) {
    if (item.quantity > 1) {
      item.quantity--
    }
    renderCart(cart)
    saveLocalStorage()
  }
}

window.deteleProduct = deteleProduct
function deteleProduct(id) {

  for (let i = 0; i < cart.length; i++) {
    if (cart[i].product.id == id) {
      if (confirm('Bạn có chắc muốn xóa sản phẩm này không?')) {
        cart.splice(i, 1)
        renderCart(cart)
        saveLocalStorage()
      }

    }
  }

}

window.clearCart = clearCart
function clearCart() {
  if (confirm('Bạn có chắc muốn xóa tất cả sản phẩm không?')) {
    removeAllItems(cart)
    renderCart(cart)
    saveLocalStorage()
  }

}

window.purchase = purchase
function purchase() {
  removeAllItems(cart)
  renderCart(cart)
  saveLocalStorage()
}






