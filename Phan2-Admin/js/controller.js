let formatter = new Intl.NumberFormat("vn-VN", {
  style: "currency",
  currency: "VND",
});

export function onLoading() {
  document.getElementById('loading').style.display = 'flex';
}
export function offLoading() {
  document.getElementById('loading').style.display = 'none';
}

export const renderProductAdmin = (list) => {
  let content = "";
  list.forEach((item) => {
    content += /*HTML*/ `
        <tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td><img style="width:70px" src="${item.img}" alt=""></td>
        <td>${formatter.format(item.price)}</td>
        <td>${item.desc}</td>
        <td>
        <button onclick="suaSP(${item.id
      })" class="btn btn-warning "data-toggle="modal"
        data-target="#modalUpdate">Sửa</button>
          <!-- Modal -->
          <div
          class="modal fade"
          id="modalUpdate"
          tabindex="-1"
          role="dialog"
          aria-labelledby="modalUpdateTitle"
          aria-hidden="true"
        >
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="modalUpdateTitle">
                  Thông tin sản phẩm
                </h5>
                <button
                  type="button"
                  class="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-6">
              <div class="form-group">
              <label for="">ID</label>
              <input
               type="text"
               class="form-control"
               name=""
               id="inpIDUpdate"
               placeholder=""
               disabled="true"
              />
              </div>
              <div class="form-group">
                 <label for="">Tên sản phẩm</label>
                 <input
                   type="text"
                   class="form-control"
                   name=""
                   id="inpTenUpdate"
                   aria-describedby="helpId"
                   placeholder=""
                 />
                 <span id="tbinpTenUpdate" class="form-message"></span>
               </div>
               <div class="form-group">
                 <label for="">Hình ảnh</label>
                 <input
                   type="text"
                   class="form-control"
                   name=""
                   id="inpHinhAnhUpdate"
                   aria-describedby="helpId"
                   placeholder=""
                 />
                 <span id="tbinpHinhAnhUpdate" class="form-message"></span>
               </div>
               <div class="form-group">
                 <label for="">Giá sản phẩm</label>
                 <input
                   type="text"
                   class="form-control"
                   name=""
                   id="inpGiaUpdate"
                   aria-describedby="helpId"
                   placeholder=""
                 />
                 <span id="tbinpGiaUpdate" class="form-message"></span>
              </div>
            </div>
            <div class="col-6">
            <div class="form-group">
              <label for="">Screen</label>
              <input
                type="text"
                class="form-control"
                name=""
                id="inpScreenUpdate"
                aria-describedby="helpId"
                placeholder=""
              />
              <span id="tbinpScreenUpdate" class="form-message"></span>

            </div>
            <div class="form-group">
              <label for="">Front Camera</label>
              <input
                type="text"
                class="form-control"
                name=""
                id="inpFrontUpdate"
                aria-describedby="helpId"
                placeholder=""
              />
              <span id="tbinpFrontUpdate" class="form-message"></span>
            </div>
            <div class="form-group">
              <label for="">Back Camera</label>
              <input
                type="text"
                class="form-control"
                name=""
                id="inpBackUpdate"
                aria-describedby="helpId"
                placeholder=""
              />
              <span id="tbinpBackUpdate" class="form-message"></span>
            </div>
            <div class="form-group">
              <label for="">Mô tả</label>
              <textarea
                class="form-control"
                name=""
                id="inpMoTaUpdate"
                rows="3"
              ></textarea>
            </div>
            <div class="form-group">
              <label for="">Loại điện thoại</label>
              <select class="form-control" name="" id="inpLoaiUpdate">
                <option selected="selected">Chọn loại</option>
                <option value="iphone">Iphone</option>
                <option value="samsung">Samsung</option>
              </select>
              <span id="tbinpLoaiUpdate" class="form-message"></span>
            </div>
            </div>
          </div>
           
              </div>
              <div class="modal-footer">
                <button
                  type="button"
                  class="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Đóng
                </button>
                <button
                  onclick="capNhatSP()"
                  type="button"
                  class="btn btn-primary"
                  id="capNhatSP"
                >
                  Cập nhật
                </button>
              </div>
            </div>
          </div>
        </div>
     
<button onclick="xoaSP(${item.id})" type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
Xóa
</button>


        </td>
        </tr>
   
        `;
  });
  document.getElementById("tbodyListProducts").innerHTML = content;
};

export const layThongTinSanPham = () => {
  const name = document.getElementById("inpTen").value;
  const price = document.getElementById("inpGia").value;
  const screen = document.getElementById("inpScreen").value;
  const backCamera = document.getElementById("inpBack").value;
  const frontCamera = document.getElementById("inpFront").value;
  const img = document.getElementById("inpHinhAnh").value;
  const desc = document.getElementById("inpMoTa").value;
  const type = document.getElementById("inpLoai").value;

  return {
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    img,
    desc,
    type,
  };
};

export const showThongTinLenForm = (sanPham) => {
  let { name, price, screen, backCamera, frontCamera, img, desc, type, id } =
    sanPham;
  document.getElementById("inpTenUpdate").value = name;
  document.getElementById("inpGiaUpdate").value = price;
  document.getElementById("inpScreenUpdate").value = screen;
  document.getElementById("inpBackUpdate").value = backCamera;
  document.getElementById("inpFrontUpdate").value = frontCamera;
  document.getElementById("inpHinhAnhUpdate").value = img;
  document.getElementById("inpMoTaUpdate").value = desc;
  document.getElementById("inpLoaiUpdate").value = type;
  document.getElementById("inpIDUpdate").value = id;
};

export const layThongTinSanPhamUpdate = () => {
  const name = document.getElementById("inpTenUpdate").value;
  const price = document.getElementById("inpGiaUpdate").value;
  const screen = document.getElementById("inpScreenUpdate").value;
  const backCamera = document.getElementById("inpBackUpdate").value;
  const frontCamera = document.getElementById("inpFrontUpdate").value;
  const img = document.getElementById("inpHinhAnhUpdate").value;
  const desc = document.getElementById("inpMoTaUpdate").value;
  const type = document.getElementById("inpLoaiUpdate").value;
  const id = document.getElementById("inpIDUpdate").value;

  return {
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    img,
    desc,
    type,
    id,
  };
};
