export const validator = {
    kiemTraRong: function (valueInput, IdError) {
        if (valueInput == '') {
            document.getElementById(IdError).innerHTML = `Trường này không được rỗng`
            return false
        } else {
            document.getElementById(IdError).innerHTML = ''
            return true
        }

    },
    kiemTraLoai: function (valueInput, IdError) {
        if (valueInput == 'Chọn loại') {
            document.getElementById(IdError).innerHTML = `Vui lòng chọn loại điện thoại`
            return false
        } else {
            document.getElementById(IdError).innerHTML = ''
            return true
        }
    },
    kiemTraSo: function (valueInput, IdError) {
        var regex = /^[0-9]+$/

        if (regex.test(valueInput)) {
            document.getElementById(IdError).innerHTML = ''
            return true
        } else {
            document.getElementById(IdError).innerHTML = `Vui lòng nhập số`
            return false
        }
    },
}
