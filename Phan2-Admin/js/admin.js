import { layThongTinSanPham, layThongTinSanPhamUpdate, onLoading, renderProductAdmin, showThongTinLenForm, offLoading } from "./controller.js"
import { Products } from "./model.js"
import { validator } from "./validator.js"

const BASE_URL = 'https://62f8b754e0564480352bf3cc.mockapi.io'






function renderProductServices() {
    onLoading()
    axios({
        url: `${BASE_URL}/sp`,
        method: 'GET',
    })
        .then(res => {
            offLoading()
            renderProductAdmin(res.data)
        })
        .catch(err => {
            console.log('err: ', err);

        })
}
renderProductServices()




window.themSP = themSP
function themSP() {
    let { name, price, screen, backCamera, frontCamera, img, desc, type } = layThongTinSanPham()

    let sanPham = new Products(name, price, screen, backCamera, frontCamera, img, desc, type)
    //Validator
    let isValid = validator.kiemTraRong(name.trim(), 'tbinpTen')
    isValid = isValid & validator.kiemTraRong(img.trim(), 'tbinpHinhAnh')
    isValid = isValid & validator.kiemTraRong(price.trim(), 'tbinpGia') &&
        validator.kiemTraSo(price.trim(), 'tbinpGia')
    isValid = isValid & validator.kiemTraRong(screen.trim(), 'tbinpScreen')
    isValid = isValid & validator.kiemTraRong(frontCamera.trim(), 'tbinpFront')
    isValid = isValid & validator.kiemTraRong(backCamera.trim(), 'tbinpBack')
    isValid = isValid & validator.kiemTraRong(backCamera.trim(), 'tbinpBack')
    isValid = isValid & validator.kiemTraLoai(type.trim(), 'tbinpLoai')


    onLoading()
    if (isValid == false) {
        document.getElementById('themSP').removeAttribute('data-dismiss')
        offLoading()

        return
    } else {
        document.getElementById('themSP').setAttribute('data-dismiss', 'modal')
        axios({
            url: `${BASE_URL}/sp`,
            method: 'POST',
            data: sanPham
        })
            .then(res => {

                offLoading()
                renderProductServices()

            })
            .catch(err => {
                console.log('err: ', err);

            })
    }


}

window.xoaSP = xoaSP
function xoaSP(id) {

    // onLoading()
    axios({
        url: `${BASE_URL}/sp/${id}`,
        method: 'DELETE',
    })
        .then(res => {
            // offLoading()

            renderProductServices()


        })
        .catch(err => {
            console.log('err: ', err);

        })


}

window.suaSP = suaSP
function suaSP(id) {
    onLoading()

    axios({
        url: `${BASE_URL}/sp/${id}`,
        method: 'GET'
    })
        .then(res => {
            offLoading()

            showThongTinLenForm(res.data)
        })
        .catch(err => {
            console.log('err: ', err);

        })
}


window.capNhatSP = capNhatSP
function capNhatSP() {

    let { name, price, screen, backCamera, frontCamera, img, desc, type, id } = layThongTinSanPhamUpdate()
    let sanPhamUpdate = new Products(name, price, screen, backCamera, frontCamera, img, desc, type, id)
    //Validator
    let isValid = validator.kiemTraRong(name.trim(), 'tbinpTenUpdate')
    isValid = isValid & validator.kiemTraRong(img.trim(), 'tbinpHinhAnhUpdate')
    isValid = isValid & validator.kiemTraRong(price.trim(), 'tbinpGiaUpdate') &&
        validator.kiemTraSo(price.trim(), 'tbinpGiaUpdate')
    isValid = isValid & validator.kiemTraRong(screen.trim(), 'tbinpScreenUpdate')
    isValid = isValid & validator.kiemTraRong(frontCamera.trim(), 'tbinpFrontUpdate')
    isValid = isValid & validator.kiemTraRong(backCamera.trim(), 'tbinpBackUpdate')
    isValid = isValid & validator.kiemTraLoai(type.trim(), 'tbinpLoaiUpdate')
    onLoading()
    if (isValid == false) {
        document.getElementById('capNhatSP').removeAttribute('data-dismiss')
        offLoading()

        return
    } else {
        document.getElementById('capNhatSP').setAttribute('data-dismiss', 'modal')
        axios({
            url: `${BASE_URL}/sp/${id}`,
            method: 'PUT',
            data: sanPhamUpdate
        })
            .then(res => {
                offLoading()
                renderProductServices()

            })
            .catch(err => {
                console.log('err: ', err);

            })
    }

}

